package programa;

import java.util.Scanner;
import clases.FicheroRaf;
import clases.FicheroSecuencial;
import clases.GestorFicheros;

/**
 * Clase Programa que permite al usuario interactuar con la aplicaci�n mediante
 * un men�. Los datos necesarios se introducen por consola.
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Programa {

	static Scanner input = new Scanner(System.in);
	static GestorFicheros listaFicherosCreados = new GestorFicheros();
	static String opcion;

	public static void main(String[] args) {

		boolean esOpcionValida = true;

		do {

			esOpcionValida = iniciarPrograma();

		} while (!esOpcionValida);

	}

	private static boolean iniciarPrograma() {

		opcion = mostrarMenuInicial();

		switch (opcion) {

		case "0":
			terminarPrograma();
			break;

		case "1":
			iniciarProgramaFicherosSecuenciales();
			break;

		case "2":
			iniciarProgramaFicherosRaf();
			break;

		default:
			System.out.println("La opci�n '" + opcion + "' no es v�lida");
			return false;

		}

		return true;

	}

	private static void iniciarProgramaFicherosSecuenciales() {

		String opcion = "";
		FicheroSecuencial fichero = crearFicheroSecuencial(pedirNombreFichero());

		do {

			opcion = mostrarMenuFicherosSecuenciales();
			iniciarProgramaFicherosSecuenciales(opcion, fichero);

		} while (!opcion.equals("1")); // 1 para volver al men� principal

	}

	private static void iniciarProgramaFicherosRaf() {

		String opcion = "";
		FicheroRaf fichero = crearFicheroRaf(pedirNombreFichero());

		do {

			opcion = mostrarMenuFicherosAccesoAleatorio();
			iniciarProgramaFicherosAccesoAleatorio(opcion, fichero);

		} while (!opcion.equals("1")); // 1 para volver al men� principal

	}

	private static boolean iniciarProgramaFicherosSecuenciales(String opcion, FicheroSecuencial fichero) {

		switch (opcion) {

		case "0":
			terminarPrograma();
			break;

		case "1":
			// Volver al men� principal
			iniciarPrograma();
			break;

		case "2":
			// Crear fichero
			fichero.altaMonumentos();
			fichero.crearFichero();
			break;

		case "3":
			// Mostrar monumentos del fichero
			fichero.mostrarFichero();
			break;

		case "4":
			// Buscar monumento por c�digo
			fichero.buscarMonumento();
			break;

		case "5":
			// Mostrar cantidad de monumentos en una ciudad
			fichero.contarMonumentosCiudad();
			break;

		case "6":
			// Mostrar monumentos con entrada gratis
			fichero.mostrarMonumentosConEntradaGratis();
			break;

		case "7":
			// Organizar monumentos por c�digo, e introducirlos de nuevo en el fichero
			fichero.organizarMonumentosPorCodigo();
			break;

		case "8":
			// Eliminar un monumento del fichero
			fichero.eliminarMonumentoDelFichero();
			break;

		case "9":
			// Invertir el texto de derecha a izquierda
			fichero.cambiarDireccionLineas();
			break;

		default:
			System.out.println("La opci�n '" + opcion + "' no es v�lida");
			return false;

		}

		return true;

	}

	private static boolean iniciarProgramaFicherosAccesoAleatorio(String opcion, FicheroRaf fichero) {

		switch (opcion) {

		case "0":
			terminarPrograma();
			break;

		case "1":
			// Volver al men� principal
			iniciarPrograma();
			break;

		case "2":
			// Escribir
			fichero.altaMonumentos();
			fichero.escribirFichero();
			break;

		case "3":
			// Mostrar
			fichero.mostrarInformacion();
			break;

		case "4":
			// Modificar
			fichero.modificarFichero();
			break;

		case "5":
			// Extra n� 1 - Organizar lista de monumentos, volver a introducirla
			fichero.organizarMonumentosPorCodigo();
			break;

		case "6":
			// Extra n� 2 - Sumar IVA al precio
			fichero.sumarIvaAlPrecio();
			break;

		case "7":
			// Extra n� 3 - Cambiar texto a may�sculas
			fichero.cambiarTextoMayusculas();
			break;

		case "8":
			// Extra n� 4 - Cambiar texto a min�sculas
			fichero.cambiarTextoMinusculas();
			break;

		case "9":
			// Extra n� 5 - Organizar monumentos por precio
			fichero.mostrarInformacionEnUnaLinea();
			break;

		case "10":
			// Extra n� 6 - Eliminar contenido del fichero
			fichero.eliminarContenido();
			break;

		default:
			System.out.println("La opci�n '" + opcion + "' no es v�lida");
			return false;

		}

		return true;

	}

	private static String pedirNombreFichero() {

		System.out.println("Introduce el nombre del fichero con el que quieres trabajar:");
		String nombre = input.nextLine();

		return nombre;

	}

	private static FicheroSecuencial crearFicheroSecuencial(String nombreNuevoFichero) {

		FicheroSecuencial fichero = null;

		while (listaFicherosCreados.existeFicheroRaf(nombreNuevoFichero)) {

			System.out.println("AVISO: YA EXISTE UN FICHERO RAF CON EL NOMBRE '" + nombreNuevoFichero + "'");
			System.out.println("INTRODUCE OTRO NOMBRE PARA CREAR EL FICHERO SECUENCIAL\n");
			nombreNuevoFichero = pedirNombreFichero();

		}

		if (!listaFicherosCreados.existeFicheroSecuencial(nombreNuevoFichero)) {

			fichero = new FicheroSecuencial(nombreNuevoFichero);
			listaFicherosCreados.getListaFicherosSecuencialesCreados().add(fichero);

		} else {

			fichero = listaFicherosCreados.buscarFicheroSecuencial(nombreNuevoFichero);

		}

		return fichero;

	}

	private static FicheroRaf crearFicheroRaf(String nombreNuevoFichero) {

		FicheroRaf fichero = null;

		while (listaFicherosCreados.existeFicheroSecuencial(nombreNuevoFichero)) {

			System.out.println("AVISO: YA EXISTE UN FICHERO SECUENCIAL CON EL NOMBRE '" + nombreNuevoFichero + "'");
			System.out.println("INTRODUCE OTRO NOMBRE PARA CREAR EL FICHERO RAF\n");
			nombreNuevoFichero = pedirNombreFichero();

		}

		if (!listaFicherosCreados.existeFicheroRaf(nombreNuevoFichero)) {

			fichero = new FicheroRaf(nombreNuevoFichero);

			listaFicherosCreados.getListaFicherosRafCreados().add(fichero);

		} else {

			fichero = listaFicherosCreados.buscarFicheroRaf(nombreNuevoFichero);

		}

		return fichero;

	}

	private static String mostrarMenuInicial() {

		System.out.println("______________________________________________");
		System.out.println("               MEN� PRINCIPAL");
		System.out.println("______________________________________________");
		System.out.println("\n  ELIGE UNA OPCI�N DEL MEN�:");
		System.out.println("  0 - TERMINAR PROGRAMA");
		System.out.println("  1 - Men� ficheros secuenciales");
		System.out.println("  2 - Men� ficheros de acceso aleatorio");
		return input.nextLine();

	}

	private static String mostrarMenuFicherosSecuenciales() {

		System.out.println("______________________________________________");
		System.out.println("          MEN� FICHEROS SECUENCIALES");
		System.out.println("______________________________________________");
		System.out.println("\n  ELIGE UNA OPCI�N DEL MEN�:");
		System.out.println("  0 - TERMINAR PROGRAMA");
		System.out.println("  1 - VOLVER AL MEN� PRINCIPAL");
		System.out.println("  2 - Crear fichero con la lista de monumentos");
		System.out.println("  3 - Mostrar fichero");
		System.out.println("  4 - Buscar monumento por c�digo");
		System.out.println("  5 - EXTRA n� 1, Mostrar cantidad de monumentos en una ciudad");
		System.out.println("  6 - EXTRA n� 2, Mostrar monumentos con entrada gratis");
		System.out.println("  7 - EXTRA n� 3, Organizar monumentos por c�digo, e introducirlos de nuevo en el fichero");
		System.out.println("  8 - EXTRA n� 4, Eliminar un monumento del fichero");
		System.out.println("  9 - EXTRA n� 5, Invertir el texto de derecha a izquierda");

		return input.nextLine();

	}

	private static String mostrarMenuFicherosAccesoAleatorio() {

		System.out.println("______________________________________________");
		System.out.println("      MEN� FICHEROS DE ACCESO ALEATORIO");
		System.out.println("______________________________________________");
		System.out.println("\n  ELIGE UNA OPCI�N DEL MEN�:");
		System.out.println("  0  - TERMINAR PROGRAMA");
		System.out.println("  1  - VOLVER AL MEN� PRINCIPAL");
		System.out.println("  2  - Escribir en fichero");
		System.out.println("  3  - Mostrar informaci�n");
		System.out.println("  4  - Modificar el precio");
		System.out.println("  5  - EXTRA n� 1, Organizar monumentos por c�digo");
		System.out.println("  6  - EXTRA n� 2, A�adir el IVA a un precio");
		System.out.println("  7  - EXTRA n� 3, Cambiar texto a may�sculas");
		System.out.println("  8  - EXTRA n� 4, Cambiar texto a min�sculas");
		System.out.println("  9  - EXTRA n� 5, Mostrar monumentos en una l�nea");
		System.out.println("  10 - EXTRA n� 6, Eliminar contenido del fichero");

		return input.nextLine();

	}

	private static void terminarPrograma() {

		System.out.println("______________________________________________");
		System.out.println("            PROGRAMA TERMINADO");
		System.out.println("______________________________________________");
		listaFicherosCreados.eliminarFicherosCreados();
		input.close();
		System.exit(0);

	}

}
