package clases;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * Clase Gestor ficheros, que almacena informaci�n de los ficheros creados.
 * Contiene m�todos de gesti�n de ficheros (eliminar, buscar, combrobar
 * existencia de ficheros).
 * 
 * @author Klara Galuskova, 1� DAW
 *
 */

public class GestorFicheros {

	private ArrayList<FicheroRaf> listaFicherosRafCreados;
	private ArrayList<FicheroSecuencial> listaFicherosSecuencialesCreados;

	public GestorFicheros() {

		this.listaFicherosSecuencialesCreados = new ArrayList<FicheroSecuencial>();
		this.listaFicherosRafCreados = new ArrayList<FicheroRaf>();

	}

	public ArrayList<FicheroRaf> getListaFicherosRafCreados() {
		return listaFicherosRafCreados;
	}

	public void setListaFicherosRafCreados(ArrayList<FicheroRaf> listaFicherosRafCreados) {
		this.listaFicherosRafCreados = listaFicherosRafCreados;
	}

	public ArrayList<FicheroSecuencial> getListaFicherosSecuencialesCreados() {
		return listaFicherosSecuencialesCreados;
	}

	public void setListaFicherosSecuencialesCreados(ArrayList<FicheroSecuencial> listaFicherosSecuencialesCreados) {
		this.listaFicherosSecuencialesCreados = listaFicherosSecuencialesCreados;
	}

	public boolean existeFicheroSecuencial(String nombreNuevoFichero) {

		for (FicheroSecuencial fichero : listaFicherosSecuencialesCreados) {

			if (fichero != null && fichero.getFichero().exists()
					&& fichero.getNombreFichero().equals(nombreNuevoFichero)) {

				return true;

			}

		}

		return false;

	}

	public boolean existeFicheroRaf(String nombreNuevoFichero) {

		for (FicheroRaf fichero : listaFicherosRafCreados) {

			if (fichero != null && fichero.getNombreFichero().equals(nombreNuevoFichero)) {

				return true;

			}

		}

		return false;

	}

	public FicheroSecuencial buscarFicheroSecuencial(String nombreNuevoFichero) {

		for (FicheroSecuencial fichero : listaFicherosSecuencialesCreados) {

			if (fichero != null && fichero.getFichero().exists()
					&& fichero.getNombreFichero().equals(nombreNuevoFichero)) {

				return fichero;

			}

		}

		return null;

	}

	public FicheroRaf buscarFicheroRaf(String nombreNuevoFichero) {

		for (FicheroRaf fichero : listaFicherosRafCreados) {

			if (fichero != null && fichero.getNombreFichero().equals(nombreNuevoFichero)) {

				return fichero;

			}

		}

		return null;

	}

	public void eliminarFicherosCreados() {

		eliminarFicherosSecuencialesCreados();
		eliminarFicherosRafCreados();

	}

	private void eliminarFicherosSecuencialesCreados() {

		Iterator<FicheroSecuencial> iterator = listaFicherosSecuencialesCreados.iterator();

		while (iterator.hasNext()) {

			FicheroSecuencial fichero = iterator.next();

			if (fichero.getFichero().exists()) {
				fichero.getFichero().delete();
			}

			iterator.remove();

		}

	}

	private void eliminarFicherosRafCreados() {

		Iterator<FicheroRaf> iterator = listaFicherosRafCreados.iterator();

		while (iterator.hasNext()) {

			FicheroRaf fichero = iterator.next();

			File ficheroBorrar = new File(fichero.getNombreFichero());
			ficheroBorrar.delete();

			iterator.remove();

		}

	}

}
