package clases;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Collections;

/**
 * 
 * Clase FicheroRaf, hereda de la clase GestorMonumentos. Contiene m�todos para
 * trabajar con ficheros de acceso aleatorio.
 * 
 * @author Klara Galuskova, 1� DAW
 *
 */

public class FicheroRaf extends GestorMonumentos {

	private String nombreFichero;

	public FicheroRaf(String nombreFichero) {

		super();
		this.nombreFichero = nombreFichero;

	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	/**
	 * M�todo para dar de alta a monumentos. Llama al m�todo con el mismo nombre de
	 * la clase padre.
	 */
	public void altaMonumentos() {

		super.altaMonumentos(this.nombreFichero);

	}

	/**
	 * M�todo no recibe nada y no devuelve nada. Crea el contenido del fichero de
	 * acceso aleatorio - sobreescribe el contenido anterior.
	 */
	public void escribirFichero() {

		RandomAccessFile fichero = null;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			fichero.seek(fichero.getFilePointer());

			for (Monumento monumento : this.listaMonumentos) {

				fichero.writeUTF(formatearCadena(monumento.getCodigo(), 5));
				fichero.writeUTF(formatearCadena(monumento.getNombre(), 20));
				fichero.writeUTF(formatearCadena(monumento.getCiudad(), 20));
				fichero.writeDouble(monumento.getPrecioEntrada());

			}

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * M�todo que no recibe nada y no devuelve nada. Muestra el contenido del
	 * fichero de acceso aleatorio.
	 */
	public void mostrarInformacion() {

		RandomAccessFile fichero = null;

		String codigo;
		String nombre;
		String ciudad;
		double precio;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			boolean finFichero = false;

			if (fichero.length() > 0) {

				System.out.println("    LISTA DE MONUMENTOS");

			}

			do {

				try {

					codigo = fichero.readUTF();
					nombre = fichero.readUTF();
					ciudad = fichero.readUTF();
					precio = (double) fichero.readDouble();

					System.out.println("___________________________");
					System.out.println("  C�digo monumento: " + codigo);
					System.out.println("  Nombre: " + nombre);
					System.out.println("  Ciudad: " + ciudad);
					System.out.println("  Precio: " + precio);
					System.out.println("___________________________");

				} catch (EOFException e) {

					if (fichero.length() == 0) {

						System.out.println("EL FICHERO EST� VAC�O");

					} else {

						System.out.println("\n            FIN");

					}

					finFichero = true;

				}

			} while (!finFichero);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}
	}

	/**
	 * M�todo para modificar el precio de un monumento. El c�digo del monumento se
	 * pide por teclado, al igual que el nuevo precio. El m�todo no devuelve nada, y
	 * no recibe nada. Luego lo cambia tanto en el fichero, como en la lista de
	 * monumentos.
	 */
	public void modificarFichero() {

		RandomAccessFile fichero = null;
		boolean finFichero = false;

		String codigoMonumentoModificar = "";
		String codigo;
		double nuevoPrecio = 0;
		boolean monumentoEncontrado = false;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			if (fichero.length() > 0) {

				System.out.println("Introduce el c�digo del monumento cuyo precio quieres modificar:");
				codigoMonumentoModificar = input.nextLine();

			}

			do {

				try {

					codigo = fichero.readUTF();
					fichero.readUTF();
					fichero.readUTF();
					fichero.readDouble();

					if (codigo.equalsIgnoreCase(formatearCadena(codigoMonumentoModificar, 5))) {

						monumentoEncontrado = true;

						boolean error = false;

						do {

							try {

								System.out.println("Introduce el nuevo precio:");
								nuevoPrecio = Double.parseDouble(input.nextLine());
								error = false;

							} catch (java.lang.NumberFormatException e) {

								error = true;
								System.out.println("ERROR: FORMATO DE PRECIO INCORRECTO");

							}

						} while (error);

						super.buscarMonumento(codigoMonumentoModificar).setPrecioEntrada(nuevoPrecio);
						System.out.println("PRECIO CAMBIADO -> NUEVO PRECIO: " + nuevoPrecio + " �");

						fichero.seek(fichero.getFilePointer() - 8);
						fichero.writeDouble(nuevoPrecio);
						fichero.readDouble();

					}

				} catch (EOFException e) {

					if (fichero.length() == 0) {

						System.out.println("EL FICHERO EST� VAC�O");

					}

					finFichero = true;

				}

			} while (!finFichero && !monumentoEncontrado);

			if (fichero.length() > 0 && !monumentoEncontrado) {

				System.out.println("MONUMENTO '" + codigoMonumentoModificar + "' NO ENCONTRADO");

			}

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * M�todo que organiza los monumentos por c�digo, y vuelve a introducirlos en el
	 * fichero. Hace uso del m�todo {@code escribirFichero()}. El m�todo no recibe
	 * nada y no devuelve nada.
	 */
	public void organizarMonumentosPorCodigo() {

		if (this.listaMonumentos != null && this.listaMonumentos.size() > 0) {

			Collections.sort(this.listaMonumentos);
			escribirFichero();

			System.out.println("MONUMENTOS ORGANIZADOS");

		} else if (this.listaMonumentos != null) {

			System.out.println("NO HAY MONUMENTOS PARA ORGANIZAR");

		}

	}

	/**
	 * El m�todo suma el IVA al precio del monumento cuyo c�digo pide por teclado.
	 * No recibe nada y no devuelve nada. Luego lo cambia tanto en el fichero, como
	 * en la lista de monumentos.
	 */
	public void sumarIvaAlPrecio() {

		RandomAccessFile fichero = null;
		boolean finFichero = false;

		String codigoMonumentoModificar = "";
		String codigo;
		double nuevoPrecio;
		boolean monumentoEncontrado = false;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			if (fichero.length() > 0) {

				System.out.println("Introduce el c�digo del monumento cuyo precio quieres modificar:");
				codigoMonumentoModificar = input.nextLine();

			}

			do {

				try {

					codigo = fichero.readUTF();
					fichero.readUTF();
					fichero.readUTF();
					fichero.readDouble();

					if (codigo.equalsIgnoreCase(formatearCadena(codigoMonumentoModificar, 5))) {

						monumentoEncontrado = true;

						nuevoPrecio = super.buscarMonumento(codigoMonumentoModificar).sumarIvaAlPrecio();

						super.buscarMonumento(codigoMonumentoModificar).setPrecioEntrada(nuevoPrecio);

						System.out.println("PRECIO CAMBIADO -> PRECIO CON IVA: " + nuevoPrecio + " �");

						fichero.seek(fichero.getFilePointer() - 8);
						fichero.writeDouble(nuevoPrecio);
						fichero.readDouble();

					}

				} catch (EOFException e) {

					if (fichero.length() == 0) {

						System.out.println("EL FICHERO EST� VAC�O");

					}

					finFichero = true;

				}

			} while (!finFichero && !monumentoEncontrado);

			if (fichero.length() > 0 && !monumentoEncontrado) {

				System.out.println("MONUMENTO '" + codigoMonumentoModificar + "' NO ENCONTRADO");

			}

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * El m�todo no recibe nada y no devuelve nada. Cambia todo el texto del fichero
	 * a may�sculas, y vuelve a introducir el contenido modificado en el fichero.
	 */
	public void cambiarTextoMayusculas() {

		RandomAccessFile fichero = null;
		boolean finFichero = false;

		String codigo;
		String nombre;
		String ciudad;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			do {

				try {

					codigo = fichero.readUTF().toUpperCase();
					fichero.seek(fichero.getFilePointer() - 7);
					fichero.writeUTF(formatearCadena(codigo, 5));

					nombre = fichero.readUTF().toUpperCase();
					fichero.seek(fichero.getFilePointer() - 22);
					fichero.writeUTF(formatearCadena(nombre, 20));

					ciudad = fichero.readUTF().toUpperCase();
					fichero.seek(fichero.getFilePointer() - 22);
					fichero.writeUTF(formatearCadena(ciudad, 20));

					fichero.readDouble();

				} catch (EOFException e) {

					if (fichero.length() == 0) {

						System.out.println("EL FICHERO EST� VAC�O");

					} else {

						System.out.println("TEXTO CAMBIADO A MAY�SCULAS");

					}

					finFichero = true;

				}

			} while (!finFichero);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * El m�todo no recibe nada y no devuelve nada. Cambia todo el texto del fichero
	 * a min�sculas, y vuelve a introducir el contenido modificado en el fichero.
	 */
	public void cambiarTextoMinusculas() {

		RandomAccessFile fichero = null;
		boolean finFichero = false;

		String codigo;
		String nombre;
		String ciudad;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			do {

				try {

					codigo = fichero.readUTF().toLowerCase();
					fichero.seek(fichero.getFilePointer() - 7);
					fichero.writeUTF(formatearCadena(codigo, 5));

					nombre = fichero.readUTF().toLowerCase();
					fichero.seek(fichero.getFilePointer() - 22);
					fichero.writeUTF(formatearCadena(nombre, 20));

					ciudad = fichero.readUTF().toLowerCase();
					fichero.seek(fichero.getFilePointer() - 22);
					fichero.writeUTF(formatearCadena(ciudad, 20));

					fichero.readDouble();

				} catch (EOFException e) {

					if (fichero.length() == 0) {

						System.out.println("EL FICHERO EST� VAC�O");

					} else {

						System.out.println("TEXTO CAMBIADO A MIN�SCULAS");

					}

					finFichero = true;

				}

			} while (!finFichero);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}

	}

	/**
	 * M�todo que no recibe nada y no devuelve nada. Muestra los monumentos del
	 * fichero en l�neas separadas, cada uno en una l�nea.
	 */
	public void mostrarInformacionEnUnaLinea() {

		RandomAccessFile fichero = null;

		String codigo;
		String nombre;
		String ciudad;
		double precio;
		int counter = 0;

		try {

			fichero = new RandomAccessFile(this.nombreFichero, "rw");

			boolean finFichero = false;

			do {

				try {

					codigo = fichero.readUTF();
					nombre = fichero.readUTF();
					ciudad = fichero.readUTF();
					precio = (double) fichero.readDouble();

					counter++;

					System.out.println(counter + ". C�digo monumento: " + codigo.trim() + ", Nombre: " + nombre.trim()
							+ ", Ciudad: " + ciudad.trim() + ", Precio: " + precio + "�");

				} catch (EOFException e) {

					if (fichero.length() == 0) {

						System.out.println("EL FICHERO EST� VAC�O");

					}

					finFichero = true;

				}

			} while (!finFichero);

		} catch (IOException e) {

			System.out.println(MENSAJE_IOEXCEPTION);

		} finally {

			if (fichero != null) {

				try {
					fichero.close();
				} catch (IOException e) {
					System.out.println(MENSAJE_IOEXCEPTION);
				}

			}

		}
	}

	/**
	 * M�todo para eliminar el contenido del fichero. No recibe nada y no devuelve
	 * nada.
	 */
	public void eliminarContenido() {

		File aux = new File(this.nombreFichero);

		if (aux.exists()) {

			RandomAccessFile fichero = null;

			try {

				fichero = new RandomAccessFile(aux, "rw");

				fichero.setLength(0);

				System.out.println("CONTENIDO ELIMINADO");

			} catch (FileNotFoundException e) {

				System.out.println(MENSAJE_FILENOTFOUNDEXCEPTION);

			} catch (IOException e) {

				System.out.println(MENSAJE_IOEXCEPTION);

			}

		} else {

			System.out.println("FICHERO NO EXISTE");

		}

		this.listaMonumentos.clear();

	}

	/**
	 * M�todo privado para formatear la cadena que se va introducir en el fichero de
	 * acceso aleatorio. Recibe dos par�metros - la cadena original, y su longitud,
	 * y devuelve la cadena modificadad. Seg�n la longitud de la cadena original
	 * a�ade o elimina car�cteres, para que la cadena resultante tenga la longitud
	 * deseada.
	 * 
	 * @param cadena   String
	 * @param longitud int
	 * @return cadena cuya longitud es la longitud recibida por par�metro
	 */
	private String formatearCadena(String cadena, int longitud) {

		if (cadena.length() > longitud) {

			return cadena.substring(0, longitud);

		} else {

			for (int i = cadena.length(); i < longitud; i++) {
				cadena += " ";
			}

		}

		return cadena;

	}

}
