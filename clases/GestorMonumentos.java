package clases;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Clase GestorMonumentos. Contiene m�todos que permiten dar de alta a una nueva
 * instancia de la clase Monumento. Contiene la lista de todos los monumentos
 * dados de alta.
 * 
 * @author Klara Galuskova, 1� DAW
 *
 */

public class GestorMonumentos {

	protected static final String MENSAJE_IOEXCEPTION = "Error de entrada/salida de datos";
	protected static final String MENSAJE_FILENOTFOUNDEXCEPTION = "Fichero no encontrado";

	static Scanner input = new Scanner(System.in);

	protected ArrayList<Monumento> listaMonumentos;

	public GestorMonumentos() {

		this.listaMonumentos = new ArrayList<Monumento>();

	}

	public ArrayList<Monumento> getListaMonumentos() {
		return listaMonumentos;
	}

	public void setListaMonumentos(ArrayList<Monumento> listaMonumentos) {
		this.listaMonumentos = listaMonumentos;
	}

	public void altaMonumentos(String nombreFichero) {

		char opcion = 'S';

		if (this.getListaMonumentos().size() > 0) {

			do {

				System.out.println("Cantidad de monumentos en el fichero '" + nombreFichero + "': "
						+ this.getListaMonumentos().size() + " \n�Quieres introducir m�s monumentos? (S/N)");
				opcion = input.nextLine().toUpperCase().charAt(0);

				if (opcion != 'S' && opcion != 'N') {
					System.out.println("La opci�n '" + opcion + "' no es v�lida");
				}

			} while (opcion != 'S' && opcion != 'N');

		}

		if (opcion == 'S' || this.getListaMonumentos().size() == 0) {

			this.altaNuevoMonumento();

		}

	}

	private void altaNuevoMonumento() {

		char opcion;

		String codigo;
		String nombreMonumento;
		String ciudad;
		double precio = 0;

		do {

			do {

				System.out.println("Introduce el c�digo del monumento:");
				System.out.println("AVISO: MAXIMO 5 CAR�CTERES");
				codigo = input.nextLine().trim();

				if (codigo.length() > 5) {
					codigo = formatearCodigo(codigo, 5);
				}

				if (existeMonumento(codigo)) {
					System.out.println("AVISO: C�DIGO DUPLICADO. INTRODUCE OTRO C�DIGO");
				}

			} while (existeMonumento(codigo));

			System.out.println("Introduce el nombre del monumento:");
			nombreMonumento = input.nextLine();
			System.out.println("Introduce la ciudad:");
			ciudad = input.nextLine();

			boolean error = false;

			do {

				try {

					System.out.println("Introduce el precio de las entradas:");
					precio = input.nextDouble();
					error = false;

				} catch (InputMismatchException e) {

					error = true;
					input.nextLine();
					System.out.println("AVISO: EL FORMATO DE PRECIO NO ES CORRECTO");

				}

			} while (error);

			listaMonumentos.add(new Monumento(codigo, nombreMonumento, ciudad, precio));

			input.nextLine();

			do {

				System.out.println("�Quieres introducir m�s datos? (S/N)");
				opcion = input.nextLine().toUpperCase().charAt(0);

				if (opcion != 'S' && opcion != 'N') {
					System.out.println("La opci�n '" + opcion + "' no es v�lida");
				}

			} while (opcion != 'S' && opcion != 'N');

		} while (opcion == 'S');

	}

	public boolean existeMonumento(String codigo) {

		for (Monumento monumento : this.listaMonumentos) {

			if (monumento.getCodigo().equalsIgnoreCase(codigo)) {
				return true;
			}

		}

		return false;

	}

	public Monumento buscarMonumento(String codigo) {

		for (Monumento monumento : this.listaMonumentos) {

			if (monumento.getCodigo().equalsIgnoreCase(codigo)) {
				return monumento;
			}

		}

		return null;

	}

	public boolean eliminarMonumento(String codigo) {

		Iterator<Monumento> iterator = listaMonumentos.iterator();

		while (iterator.hasNext()) {

			if (iterator.next().getCodigo().equals(codigo)) {

				iterator.remove();
				return true;

			}

		}

		return false;

	}

	private String formatearCodigo(String codigo, int longitud) {

		if (codigo.length() > longitud) {

			return codigo.substring(0, longitud);

		}

		return codigo;

	}

}
