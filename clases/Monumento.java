package clases;

import java.time.LocalDate;

/**
 * 
 * Clase Monumento, implementa Comparable para ordenar objetos de clase
 * Monumento por su c�digo
 * 
 * @author Klara Galuskova, 1� DAW
 *
 */

public class Monumento implements Comparable<Monumento> {

	private static final double IVA = 0.21;

	private String codigo;
	private String nombre;
	private String ciudad;
	private double precioEntrada;
	private LocalDate fechaAlta;

	public Monumento(String codigo, String nombre, String ciudad, double precioEntrada) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.precioEntrada = precioEntrada;
		this.fechaAlta = LocalDate.now();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public double getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public int compareTo(Monumento monumento) {

		return this.codigo.compareTo(monumento.getCodigo());

	}

	public double sumarIvaAlPrecio() {

		return this.precioEntrada += (precioEntrada * IVA);

	}

	@Override
	public String toString() {
		return "Monumento - c�digo: " + codigo + ", nombre: " + nombre + ", ciudad: " + ciudad + ", precio entrada: "
				+ precioEntrada + " �, fecha de alta: " + fechaAlta;
	}

}
